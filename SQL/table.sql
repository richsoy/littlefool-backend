DROP TABLE IF EXISTS user;
CREATE TABLE user (
    id INT AUTO_INCREMENT COMMENT '主键id',
    tenant VARCHAR(20) NOT NULL COMMENT '租户',
    uid VARCHAR(50) NOT NULL COMMENT '用户id',
    name VARCHAR(50) NOT NULL COMMENT '用户昵称',
    head_url VARCHAR(150) NULL COMMENT '头像地址',
    gender VARCHAR(50) NULL COMMENT '性别',
    age VARCHAR(50) NULL COMMENT '年龄',
    province VARCHAR(20) NULL COMMENT '省',
    city VARCHAR(50) NULL COMMENT '市',
    area VARCHAR(50) NULL COMMENT '区',
    creator VARCHAR(50) NULL COMMENT '创建人',
    last_modified VARCHAR(50) NOT NULL COMMENT '最后修改人',
    create_time TIMESTAMP NOT NULL COMMENT '创建时间',
    modify_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户信息表';


DROP TABLE IF EXISTS game_data;
CREATE TABLE game_data (
    id INT AUTO_INCREMENT COMMENT '主键id',
    uid VARCHAR(50) NOT NULL COMMENT '用户id',
    new_score INT NOT NULL COMMENT '最新得分',
    top_score INT NOT NULL COMMENT '最高得分',
    score_info VARCHAR(200) NULL COMMENT '得分信息',
    view_ad_times INT NOT NULL COMMENT '观看广告次数',
    play_times INT NOT NULL COMMENT '游戏次数',
    share_times INT NOT NULL COMMENT '分享次数',
    data VARCHAR(500) NULL COMMENT '其他游戏数据',
    creator VARCHAR(50) NOT NULL COMMENT '创建人',
    last_modified VARCHAR(50) NOT NULL COMMENT '最后修改人',
    create_time TIMESTAMP NOT NULL COMMENT '创建时间',
    modify_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='游戏数据表';

