package com.littlefool.utils;

import cn.hutool.core.util.StrUtil;

import java.util.List;

/**
 * @author zhanghao
 * @description
 * @date 2024/4/18 13:43
 **/
public class StringTools {

    public static String EMPTY = "";

    /**
     * 获取字符串数组指定索引的值
     * @param toSplit 输入字符串
     * @param delimiter 分隔符
     * @param index 索引
     * @return 字符串数组指定索引的值
     */
    public static String splitGetString(String toSplit, String delimiter, int index) {
        if (StrUtil.isEmpty(toSplit) || StrUtil.isEmpty(delimiter) || index < 0) {
            return EMPTY;
        }
        List<String> splitArr = StrUtil.split(toSplit, delimiter);
        if (splitArr.isEmpty()) {
            return EMPTY;
        }
        return splitArr.get(index);
    }
}
