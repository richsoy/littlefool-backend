package com.littlefool.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author zhanghao
 * @description
 * @date 2024/4/14 14:38
 **/
@ApiModel("游戏结果")
public class GameResult {

    /**
     * 用户id
     */
    @ApiModelProperty(notes = "用户id", required = true, example = "123456")
    private String uid;

    /**
     * 游戏时长
     */
    @ApiModelProperty(notes = "游戏时长（秒）", required = true, example = "120")
    private String playTime;

    /**
     * 游戏得分
     */
    @ApiModelProperty(notes = "游戏得分", required = true, example = "80")
    private Integer score;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getPlayTime() {
        return playTime;
    }

    public void setPlayTime(String playTime) {
        this.playTime = playTime;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }
}
