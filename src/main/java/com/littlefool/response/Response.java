package com.littlefool.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.Parameter;

/**
 * @author zhanghao
 * @description
 * @date 2024/4/12 15:40
 **/
@ApiModel("基本响应结果")
public class Response {

    /**
     * 错误码，200表示成功
     */
    @ApiModelProperty(notes = "错误码", required = true, example = "200")
    private String code;

    /**
     * 错误描述
     */
    @ApiModelProperty(notes = "错误描述", required = true, example = "成功")
    private String desc;

    public Response() {
        this.code = "200";
        this.desc = "成功";
    }

    /**
     * 构造函数
     * @param code 错误码
     * @param desc 错误描述
     */
    public Response(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    /**
     * 静态方法返回响应
     * @return Response 响应数据
     */
    public static Response buildResponse() {
        return new Response();
    }

    /**
     * 静态方法返回租户错误响应
     * @return Response 响应数据
     */
    public static Response buildTenantError() {
        return new Response("400", "租户不存在");
    }

    /**
     * 静态方法返回响应
     * @param code 错误码
     * @param desc 错误描述
     * @return Response 响应数据
     */
    public static Response buildResponse(String code, String desc) {
        return new Response(code, desc);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
