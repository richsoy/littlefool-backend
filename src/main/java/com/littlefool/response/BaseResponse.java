package com.littlefool.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author zhanghao
 * @description
 * @date 2024/4/12 09:23
 **/
@ApiModel("响应结果")
public class BaseResponse<T> extends Response {

    /**
     * 泛型，返回响应数据
     */
    @ApiModelProperty(notes = "响应数据", required = true)
    private T data;

    /**
     * 构造函数
     */
    public BaseResponse() {
        this.setCode("200");
        this.setDesc("成功");
        this.data = null;
    }

    /**
     * 构造函数
     * @param code 代码
     * @param desc 描述
     * @param data 数据
     */
    public BaseResponse(String code, String desc, T data) {
        this.setCode(code);
        this.setDesc(desc);
        this.data = data;
    }

    /**
     * 构造函数
     * @param data 数据
     */
    public BaseResponse(T data) {
        this.setCode("200");
        this.setDesc("成功");
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
