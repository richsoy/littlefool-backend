package com.littlefool.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * @author zhanghao
 * @description 历史结果
 * @date 2024/4/13 21:37
 **/
@ApiModel("历史记录")
public class HistoryRecord {

    /**
     * 用户id
     */
    @ApiModelProperty(name = "用户id", required = true)
    private String uid;

    /**
     * 次数
     */
    @ApiModelProperty(name = "游戏次数", required = true)
    private String playTimes;

    /**
     * 最新得分
     */
    @ApiModelProperty(name = "最新得分", required = true)
    private Integer newScore;

    /**
     * 最大得分
     */
    @ApiModelProperty(name = "最高得分", required = true)
    private Integer topScore;

    /**
     * 得分排序，只保留top5
     */
    @ApiModelProperty(name = "得分排序，只保留top5", required = true)
    private List<Integer> historyScore;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getPlayTimes() {
        return playTimes;
    }

    public void setPlayTimes(String playTimes) {
        this.playTimes = playTimes;
    }

    public Integer getNewScore() {
        return newScore;
    }

    public void setNewScore(Integer newScore) {
        this.newScore = newScore;
    }

    public Integer getTopScore() {
        return topScore;
    }

    public void setTopScore(Integer topScore) {
        this.topScore = topScore;
    }

    public List<Integer> getHistoryScore() {
        return historyScore;
    }

    public void setHistoryScore(List<Integer> historyScore) {
        this.historyScore = historyScore;
    }
}
