package com.littlefool.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zhanghao
 * @description
 * @date 2024/4/12 15:49
 **/
@ApiModel("用户游戏数据")
public class UserGameData {

    /**
     * 用户id
     */
    @ApiModelProperty(notes = "用户id", required = true, example = "123456")
    private String uid;

    /**
     * 最新得分
     */
    @ApiModelProperty(notes = "最新得分", required = true, example = "88")
    private Integer newScore;

    /**
     * 最高得分
     */
    @ApiModelProperty(notes = "最高得分", required = true, example = "88")
    private Integer topScore;

    /**
     * 得分情况
     */
    @ApiModelProperty(notes = "得分情况", required = true, example = "k-v记录得分情况")
    private List<Record> scoreRecord;

    /**
     * 观看广告次数
     */
    @ApiModelProperty(notes = "观看广告次数", required = true, example = "10")
    private Integer viewAdTimes;

    /**
     * 总共已玩次数
     */
    @ApiModelProperty(notes = "总共已玩次数", required = true, example = "20")
    private Integer totalPlayTimes;

    /**
     * 其他游戏数据
     */
    @ApiModelProperty(notes = "其他游戏数据", required = true, example = "k-v字符串保存其他游戏数据，用于扩展")
    private Map<String, String> gameDataMap;

    /**
     * 构造函数，初始化数据
     */
    public UserGameData() {
        this.uid = "";
        this.newScore = 0;
        this.topScore = 0;
        this.scoreRecord = new ArrayList<>();
        this.viewAdTimes = 0;
        this.totalPlayTimes = 0;
        this.gameDataMap = new HashMap<>();
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Integer getNewScore() {
        return newScore;
    }

    public void setNewScore(Integer newScore) {
        this.newScore = newScore;
    }

    public Integer getTopScore() {
        return topScore;
    }

    public void setTopScore(Integer topScore) {
        this.topScore = topScore;
    }

    public List<Record> getScoreRecord() {
        return scoreRecord;
    }

    public void setScoreRecord(List<Record> scoreRecord) {
        this.scoreRecord = scoreRecord;
    }

    public Integer getViewAdTimes() {
        return viewAdTimes;
    }

    public void setViewAdTimes(Integer viewAdTimes) {
        this.viewAdTimes = viewAdTimes;
    }

    public Integer getTotalPlayTimes() {
        return totalPlayTimes;
    }

    public void setTotalPlayTimes(Integer totalPlayTimes) {
        this.totalPlayTimes = totalPlayTimes;
    }

    public Map<String, String> getGameDataMap() {
        return gameDataMap;
    }

    public void setGameDataMap(Map<String, String> gameDataMap) {
        this.gameDataMap = gameDataMap;
    }
}
