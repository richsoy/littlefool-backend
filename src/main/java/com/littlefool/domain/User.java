package com.littlefool.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author zhanghao
 * @description 用户信息
 * @date 2024/4/12 08:50
 **/
@ApiModel("历史记录")
public class User {

    /**
     * 用户唯一id
     */
    @ApiModelProperty(notes = "用户id", required = true, example = "123456")
    private String uid;

    /**
     * 昵称姓名
     */
    @ApiModelProperty(notes = "昵称姓名", required = true, example = "帅气的张三")
    private String name;

    /**
     * 头像链接
     */
    @ApiModelProperty(notes = "头像链接", required = false, example = "http://xxx.com/xxx.jpg")
    private String headUrl;

    /**
     * 性别
     */
    @ApiModelProperty(notes = "性别", required = false, example = "男")
    private String gender;

    /**
     * 年龄
     */
    @ApiModelProperty(notes = "年龄", required = false, example = "20")
    private String age;

    /**
     * 位置
     */
    @ApiModelProperty(notes = "位置", required = false, example = "浙江省杭州市临平区xx小区")
    private String location;

    /**
     * 省
     */
    @ApiModelProperty(notes = "省", required = false, example = "浙江省")
    private String province;

    /**
     * 市
     */
    @ApiModelProperty(notes = "市", required = false, example = "杭州市")
    private String city;

    /**
     * 区
     */
    @ApiModelProperty(notes = "区", required = false, example = "临平区")
    private String area;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHeadUrl() {
        return headUrl;
    }

    public void setHeadUrl(String headUrl) {
        this.headUrl = headUrl;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }
}
