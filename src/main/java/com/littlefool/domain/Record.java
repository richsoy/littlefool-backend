package com.littlefool.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author zhanghao
 * @description
 * @date 2024/4/17 16:06
 **/
@ApiModel("得分记录")
public class Record {

    /**
     * 得分
     */
    @ApiModelProperty(name = "得分", required = true)
    private String score;

    @ApiModelProperty(name = "得分时间", required = true)
    private String scoreTime;

    @ApiModelProperty(name = "游戏时长", required = true)
    private String gameTime;

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getScoreTime() {
        return scoreTime;
    }

    public void setScoreTime(String scoreTime) {
        this.scoreTime = scoreTime;
    }

    public String getGameTime() {
        return gameTime;
    }

    public void setGameTime(String gameTime) {
        this.gameTime = gameTime;
    }
}
