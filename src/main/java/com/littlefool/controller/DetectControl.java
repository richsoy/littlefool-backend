package com.littlefool.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author zhanghao
 * @description 便于检测服务是否存在
 * @date 2024/4/11 16:29
 **/
@RestController
@RequestMapping("/detect")
@Api("服务检测")
public class DetectControl {

    /**
     * 检测服务是否存活
     * @return 返回当前时间
     */
    @ApiOperation("检测服务是否存活")
    @GetMapping("/live")
    public String live() {
        Date now = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(now);
    }
}
