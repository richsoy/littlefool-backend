package com.littlefool.controller;

import com.littlefool.domain.User;
import com.littlefool.domain.UserGameData;
import com.littlefool.enumeration.Tenant;
import com.littlefool.response.BaseResponse;
import com.littlefool.response.Response;
import com.littlefool.service.IpService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;

/**
 * @author zhanghao
 * @description
 * @date 2024/4/12 08:48
 **/
@RestController
@RequestMapping("/user/{tenant}")
@Api("用户相关接口")
public class UserController extends BaseController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Resource
    private IpService ipService;

    /**
     * 获取用户ip位置
     * @return Response
     */
    @ApiOperation("获取用户ip位置")
    @ApiResponse(code = 200, message = "成功")
    @RequestMapping(method = RequestMethod.GET, path = "/ip")
    public BaseResponse<String> ip2Region() {
        String clientIp = MDC.get("clientIp");
        logger.info("clientIp: {}", clientIp);

        String location = ipService.ip2region(clientIp);
        logger.info("location: {}", location);

        return new BaseResponse<>(location);
    }

    /**
     * 保存用户数据
     * @param user 用户信息
     * @return Response
     */
    @ApiOperation("保存用户信息")
    @ApiResponse(code = 200, message = "成功")
    @RequestMapping(method = RequestMethod.POST, path = "/login")
    public Response login(@RequestBody User user, @PathVariable("tenant") String tenant) {
        logger.info("user: {}, tenant: {}", user, tenant);
        if (Tenant.notInTenant(tenant)) {
            return Response.buildTenantError();
        }

        return Response.buildResponse();
    }

    /**
     * 获取用户游戏数据
     * @param uid 用户id
     * @return 返回用户游戏数据
     */
    @ApiOperation("获取用户游戏数据")
    @ApiResponse(code = 200, message = "成功")
    @RequestMapping(method = RequestMethod.GET, path = "/game/data")
    public BaseResponse<UserGameData> fetchUserGameData(@RequestParam(name = "uid") String uid) {
        UserGameData userGameData = new UserGameData();
        userGameData.setUid("uid12345678");
        userGameData.setScoreRecord(new ArrayList<>());
        userGameData.setNewScore(50);
        userGameData.setTopScore(90);
        userGameData.setViewAdTimes(0);
        userGameData.setTotalPlayTimes(10);

        return new BaseResponse<>(userGameData);
    }
}
