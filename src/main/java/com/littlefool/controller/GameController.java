package com.littlefool.controller;

import com.littlefool.domain.HistoryRecord;
import com.littlefool.request.GameResult;
import com.littlefool.response.BaseResponse;
import com.littlefool.response.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * @author zhanghao
 * @description
 * @date 2024/4/13 12:40
 **/
@RestController
@RequestMapping("/game/{tenant}")
@Api("游戏相关接口")
public class GameController {

    private static final Logger logger = LoggerFactory.getLogger(GameController.class);

    /**
     * 获取10*15宫格数字
     * @param uid 用户id
     * @return 返回用户游戏数据
     */
    @ApiOperation("获取用户游戏数据")
    @ApiResponse(code = 200, message = "成功")
    @RequestMapping(method = RequestMethod.GET, path = "/content")
    public BaseResponse<List<List<Integer>>> fetchGameContent(@RequestParam(value="uid") String uid) {
        List<List<Integer>> gameData = new ArrayList<>();
        for (int idx = 0; idx < 15; idx ++) {
            List<Integer> numList = new ArrayList<>();

            for (int times = 0; times < 10; times ++) {
                Random random = new Random();
                int randomNumber = random.nextInt(10);
                numList.add(randomNumber);
            }
            gameData.add(numList);
        }

        return new BaseResponse<>(gameData);
    }

    /**
     * 保存游戏结果
     * @param gameResult 游戏结果
     * @return 返回是否成功
     */
    @ApiOperation("保存游戏结果")
    @ApiResponse(code = 200, message = "成功")
    @RequestMapping(method = RequestMethod.POST, path = "/result/save")
    public Response saveGameResult(@RequestBody GameResult gameResult) {

        return new Response();
    }

    /**
     * 获取历史记录
     * @param uid 用户id
     * @return 返回历史记录
     */
    @ApiOperation("返回历史记录")
    @ApiResponse(code = 200, message = "成功")
    @RequestMapping(method = RequestMethod.GET, path = "/history")
    public BaseResponse<HistoryRecord> fetchHistoryRecord(@RequestParam(value="uid") String uid) {
        HistoryRecord historyRecord = new HistoryRecord();
        historyRecord.setUid("123456");
        historyRecord.setPlayTimes("5");
        historyRecord.setHistoryScore(Arrays.asList(88, 77, 66, 55, 44));
        historyRecord.setNewScore(77);
        historyRecord.setTopScore(88);

        return new BaseResponse<>(historyRecord);
    }
}
