package com.littlefool.controller;

import com.littlefool.enumeration.Tenant;

/**
 * @author zhanghao
 * @description
 * @date 2024/4/19 16:38
 **/
public class BaseController {

    public void tenantCheck(String tenant) {
        if (Tenant.notInTenant(tenant)) {
            Throwable throwable = new Throwable("tenant not in tenant list");
            throw new IllegalArgumentException(throwable);
        }
    }
}
