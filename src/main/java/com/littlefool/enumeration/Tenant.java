package com.littlefool.enumeration;

/**
 * @author zhanghao
 * @description
 * @date 2024/4/19 15:44
 **/
public enum Tenant {

    FOOL("fool", "小笨蛋"),
    ;

    /**
     * 租户名称
     */
    private String name;

    /**
     * 租户描述
     */
    private String desc;

    Tenant(String name, String desc) {
        this.name = name;
        this.desc = desc;
    }

    /**
     * 判断租户是否存在
     * @param tenantName 租户名称
     * @return true:存在，false:不存在
     */
    public static Boolean inTenant(String tenantName) {
        for (Tenant tenant : Tenant.values()) {
            if (tenant.getName().equals(tenantName)) {
                return true;
            }
        }

        return false;
    }

    /**
     * 判断租户是否存在
     * @param tenantName 租户名称
     * @return true:存在，false:不存在
     */
    public static Boolean notInTenant(String tenantName) {
        return !inTenant(tenantName);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
