package com.littlefool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @Author zhanghao
 * @Description SpringBoot服务启动类
 * @Date 2024年4月11日 下午16:20
 **/
@EnableSwagger2
@EnableOpenApi
@EnableWebMvc
@SpringBootApplication
public class LittleFool {
    public static void main(String[] args) {
        SpringApplication.run(LittleFool.class, args);
    }
}
