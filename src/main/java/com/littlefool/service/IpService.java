package com.littlefool.service;

/**
 * @author zhanghao
 * @description
 * @date 2024/4/18 15:43
 **/
public interface IpService {

    /**
     * 根据ip地址获取地理位置信息
     * @param ip ip地址
     * @return 地理位置信息
     */
    String ip2region(String ip);
}
