package com.littlefool.service;

import com.littlefool.component.IpSearcher;
import com.littlefool.controller.UserController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author zhanghao
 * @description
 * @date 2024/4/18 15:45
 **/
@Service
public class IpServiceImpl implements IpService {

    private static final Logger logger = LoggerFactory.getLogger(IpServiceImpl.class);

    @Resource
    private IpSearcher ipSearcher;

    @Override
    public String ip2region(String ip) {
        String location = ipSearcher.ipSearch(ip);
        logger.info("ip: {},location: {}", ip, location);
        return location;
    }
}
