#!/bin/bash

# 如果进程存在则kill掉
PID=`ps -ef | grep littlefool | grep -v grep | awk '{print $2}'`
echo "查出littlefool相关的进程PID: ${PID}"
if [ -z "$PID" ]; then
  echo "littlefool进程不存在"
else
  for item in ${PID}
    do
       kill -s 9 $item
       echo "已经kill进程: $item"
    done
fi

# 启动
nohup java -jar littlefool-backend-1.0.0-SNAPSHOT.jar >/dev/null 2>&1 &

# 检查启动是否成功
pid=`ps -ef | grep littlefool | grep -v grep | awk '{print $2}'`
if [ -z "${pid}" ]; then
  echo "littlefool进程启动失败"
else
  echo "littlefool进程启动成功，PID: ${pid}"
fi